// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'bit-time',
    appId: '1:832909950740:web:70d2ab2c87024dcf1f0c01',
    storageBucket: 'bit-time.appspot.com',
    apiKey: 'AIzaSyAn1ufs87RIqMrIOkqPJIySLygFlSc5Uno',
    authDomain: 'bit-time.firebaseapp.com',
    messagingSenderId: '832909950740',
    measurementId: 'G-7RJFZR3GLJ',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
