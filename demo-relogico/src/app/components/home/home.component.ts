import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  
  user$ = this.authService.currentUser$;

  private date = new Date();

  public hour: any;
  public minute: any;
  public second: any;
  public ampm: any;
  public day: any;

  constructor(private authService: AuthenticationService, private router: Router) { }

  ngOnInit(): void {
    setInterval(() => {
      const date = new Date();;
      this.updateDate(date);
    }, 1000); 
  }

  public getNow() {
    const now = new Date();
    return {
      hours: now.getHours() + now.getMinutes() / 60,
      minutes: now.getMinutes() * 12 / 60 + now.getSeconds() * 12 / 3600,
      seconds: now.getSeconds() * 12 / 60
    }
  }

  private updateDate(date: any){
    const hours = date.getHours();

    console.log('minha hora aqui', date.getHours());

    this.hour = hours < 10 ? '0' + hours : hours;
  
    const minutes = date.getMinutes();
    this.minute = minutes < 10 ? '0' + minutes : minutes;

    const seconds = date.getSeconds();
    this.second = seconds < 10 ? '0' + seconds : seconds;
  }

   lock(): void {
    console.log("fechar")
  }
  
   unlock(): void {
    console.log("abrir")
  }

}
