var { app, BrowserWindow } = require('electron');

let win = null;

app.on('ready', () => {
  win = new BrowserWindow({
    width: 1364,
    height: 768
  })
  win.loadURL('http://localhost:4200');

  win.on('closed', () => {
    win = null;
  })
})

app.on('activate', () => {
  if (win == null)
  createWindow();
})

app.on('window-all-closed', () =>{
  if (process.platform != 'darwin') {
    app.quit();
  }
})
